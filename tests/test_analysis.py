import unittest

from minitopic.analysis import (
    Message,
    TopKeyWords,
    TopKIps,
    Keywords,
    TopK
)


class TestTopK(unittest.TestCase):
    def test_harness(self):
        top = TopK()
        for i in xrange(0, 100):
            top.merge(i)
        top.merge(1)
        top.merge(2)
        top.merge(3)

        self.assertEqual(top.top_k(3), [1, 2, 3])
        self.assertEqual(len(top.top_k(20)), 20)


class TestTopKeyWords(unittest.TestCase):
    def test_harness(self):
        msg = Message('', '')
        msg.tokens = ['a', 'b', 'b', 'c', 'a', 'a']

        top = TopKeyWords()
        top.merge(msg)

        self.assertEqual(top.query(1), ['a'])
        self.assertEqual(top.query(2), ['a', 'b'])
        self.assertEqual(top.query(10), ['a', 'b', 'c'])


class TestTopIps(unittest.TestCase):
    def test_harness(self):
        msg1 = Message('1.2.3.4', '')
        msg1.tokens = ['a']
        msg2 = Message('2.2.2.2', '')
        msg2.tokens = ['b']

        top = TopKIps()
        # received 3 msgs from 1.2.3.4
        top.merge(msg1)
        top.merge(msg1)
        top.merge(msg1)

        # received 2 msgs from 2.2.2.2
        top.merge(msg2)
        top.merge(msg2)

        self.assertEqual(top.query(1), [('1.2.3.4', ['a'])])
        self.assertEqual(
            top.query(2), [('1.2.3.4', ['a']), ('2.2.2.2', ['b'])])


class TestKeywords(unittest.TestCase):
    def test_harness(self):
        msg = 'a'
        msgs = [
            Message('3.3.3.3', msg),
            Message('3.3.3.3', msg),
            Message('3.3.3.3', msg),
            Message('1.1.1.1', msg),
            Message('1.1.1.1', msg),
            Message('2.2.2.2', msg),
        ]
        keywords = Keywords()
        for m in msgs:
            keywords.merge(m)

        expected = ['3.3.3.3', '1.1.1.1', '2.2.2.2']
        self.assertEqual(keywords.query('a'), expected)

    def test_multiple_keywords(self):
        msg1 = Message('3.3.3.3', 'a b')
        msg2 = Message('1.1.1.1', 'b b c')

        keywords = Keywords()
        keywords.merge(msg1)
        keywords.merge(msg2)

        expected_a = ['3.3.3.3']
        expected_b = ['1.1.1.1', '3.3.3.3']
        expected_c = ['1.1.1.1']
        self.assertEqual(keywords.query('a'), expected_a)
        self.assertEqual(keywords.query('b'), expected_b)
        self.assertEqual(keywords.query('c'), expected_c)

    def test_not_exists(self):
        keywords = Keywords()
        self.assertEqual(keywords.query('not exists'), None)