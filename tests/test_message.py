import unittest

from minitopic.analysis import Message


class TestTokenize(unittest.TestCase):
    def test_simple(self):
        msg = Message('', 'a b  c\td')
        expected = ['a', 'b', 'c', 'd']
        self.assertItemsEqual(msg.tokens, expected)

    def test_symbols(self):
        msg = Message('', 'h: a?! oh...')
        expected = ['h', 'a', 'oh']
        self.assertItemsEqual(msg.tokens, expected)
