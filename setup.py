import os

from setuptools import setup, find_packages

# avoid errors on python setup.py test
#(cf. http://bugs.python.org/issue15881#msg170215)
import multiprocessing

root = os.path.abspath(os.path.dirname(__file__))
version = '0.1.0'

setup(
    name='minitopic',
    version=version,

    author='darkjh',
    install_requires=[
        'bottle'
    ],
    tests_require=[],

    package_dir={'': '.'},
    include_package_data=False,

    packages=find_packages(exclude=['tests*']),
    test_suite="nose.collector",
)