Minitopic
=========

## Overview ##

This is the first version of the application.

The application can be divided into two main components:

  - Server/API
  - Analysis tasks

Basically server/api provides a restful service for receiving messages and retrieving results.

## Server/API ##

### Message endpoint ###
Client sends plain text messages to `/message` endpoint:

    curl -XPOST --data 'hello world!' localhost:8888/message

### Top keywords endpoint ###
Top keywords list can be retrieved at `/top_keywords`

### Top IPs endpint ###
Top IPs list can be retrieved at `/top_ips`

### Keyword endpoint ###
Detailed keyword information can be retrieved at `/keyword/<keyword>` by providing the keyword string

## Analysis ##

### Message ###
Upon arrival, the user message is parsed into a `Message` object with the plain text message tokenized. The IP is also recorded.

### TopK ###
A common pattern in the analysis specification is the `TopK` analysis: give the top k frequent items of a dataset/datastream.
So this concept is abstracted into its own class and is used by analysis tasks.

### Analysis ###
The 3 analysis tasks are modeled by an `Anaysis` interface which has two methods:

  - `merge` digest a message and analyse it
  - `query` returns the analysis result at the moment

It has 3 concret implementations, one for each analysis task:

  - `TopKeywords`
  - `TopIps`
  - `Keywords`

## Limits ##

### Memory ###
All working data structures are in memory (token counts, ip counts etc.). With unbounded messages the memory will explose.

### Performance ###
The system works on a single message basis. This means that client setup an connection, sends a simple message, and then probably shuts it when the message is sent. In this kind of message streaming application, a streaming API, or a "bulk load" api is always desired.

### Distribution ###
All data is processed in a single server on a single machien, this means:

  - limited network bandwith
  - limited computation resources (cpu, ram)

Simply put, this design does not scale-out, only scale-up.

## A realistic design ##
A realistic design is well different from the first version. Probably with detailed specification.

### Limit bounds ###
It's not very realistic to analyse the whole stream from beginning. A more common setup will be analyse by hourly or daily window.

### Message queue ###
In a realistic design a message queue should sit between the Server/API and the analytic tasks, acting as an buffer.
It has multiple advatages:

  - decouple API and analysis
  - batching/mini-batching messages to improve analysis performance

A typical choise will be Apache Kafka, a distributed message queue with disk persistance and reply feature.

### Processing system ###

  - Storm
  - Spark streaming

### Result data serving ###

  - Database for detailed queries

### Probabilistic algorithm ###
Depending on specification/usecase, we may not need 100% correct data. In this case some probabilistic algorithm/data structure can be used, for example Count–min sketch.

## Useful links ##

  - [Apache Kafka](http://kafka.apache.org/)
  - [Spark Streaming](https://spark.apache.org/streaming/)
  - [Twitter Algebird](https://github.com/twitter/algebird)
