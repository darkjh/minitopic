"""Server/API module providing http server and service endpoints
"""
import os

from bottle import (
    route,
    post,
    run,
    request
)
from minitopic.analysis import Processing, Message

# analysis logic
analysis = Processing()


# message endpoint
@post('/message')
def post_message():
    """Endpoint for posting plain text message
    """
    message = request.body.getvalue()
    ip = request.environ.get('REMOTE_ADDR')
    analysis.analyse(Message(ip, message))
    return


# Admin endpoints
@route('/top_keywords')
def top_keywords():
    """Endpoint for retrieving top keyword list
    """
    return '<br/>'.join(analysis.get_top_keywords())


@route('/top_ips')
def top_ips():
    """Endpoint for retrieving top ip list
    """
    # TODO TopIpResult object
    top_ips = analysis.get_top_ips()
    top_ips = ['\t'.join((i[0], str(i[1]))) for i in top_ips]
    return '<br/>'.join(top_ips)


@route('/keyword/<keyword>')
def keyword_list(keyword):
    """Endpoint for querying detail of a keyword
    """
    detail = analysis.get_keyword(keyword)
    if detail is None:
        return 'Keyword {} does not exist ...'.format(keyword)
    return '<br/>'.join(detail)


# entry point
if __name__ == '__main__':
    port = os.environ.get('PORT', 8888)
    run(host='0.0.0.0', port=int(port), debug=True)