"""Analysis module providing analysis abstractions
"""

import abc
import re

from collections import defaultdict


class Message(object):
    """Client's message"""
    def __init__(self, ip, message):
        self.ip = ip
        self.tokens = self.tokenize(message)

    @classmethod
    def tokenize(cls, message):
        """Tokenize one message into a list of keywords
        """
        msg = message.lower().strip()
        msg = re.sub('[:;!?@#$.,\'\"]', '', msg)
        return msg.split()


class TopK(object):
    """Reusable TopK abstraction"""
    def __init__(self):
        self.elem_count = defaultdict(lambda: 0)

    def merge(self, elem):
        """Take in an element"""
        self.elem_count[elem] += 1

    def top_k(self, k):
        """Returns the top k elements according to their frequency"""
        s = sorted(
            self.elem_count,
            key=self.elem_count.get,
            reverse=True
        )
        return s[:k]


class Analysis(object):
    """Abstract class for analysis tasks
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def merge(self, msg):
        """Analyse a message
        """
        pass

    @abc.abstractmethod
    def query(self, k):
        """Query information over analyzed messages
        """
        pass


class TopKeyWords(Analysis):
    """Top keywords analysis task
    """
    def __init__(self):
        self.keywords_count = TopK()

    def merge(self, msg):
        for token in msg.tokens:
            self.keywords_count.merge(token)

    def query(self, k):
        return self.keywords_count.top_k(k)


class TopKIps(Analysis):
    """Top ips analysis task
    """
    def __init__(self):
        self.ip_keywords = defaultdict(lambda: TopKeyWords())
        self.ip_count = TopK()

    def merge(self, msg):
        self.ip_count.merge(msg.ip)
        self.ip_keywords[msg.ip].merge(msg)

    def query(self, k):
        s = self.ip_count.top_k(k)
        return [
            (ip, self.ip_keywords[ip].query(10))
            for ip in s
        ]


class Keywords(object):
    """Keyword detail analysis task
    """
    def __init__(self):
        self.keyword_ips = defaultdict(lambda: TopK())

    def merge(self, msg):
        for token in msg.tokens:
            self.keyword_ips[token].merge(msg.ip)

    def query(self, keyword):
        if keyword not in self.keyword_ips:
            return None
        return self.keyword_ips[keyword].top_k(10)


class Processing(object):
    """A manager-like class that defines the analysis pipeline
    """
    def __init__(self):
        self.top_keywords = TopKeyWords()
        self.top_ips = TopKIps()
        self.keywords = Keywords()

    def analyse(self, msg):
        self.top_keywords.merge(msg)
        self.top_ips.merge(msg)
        self.keywords.merge(msg)

    def get_top_keywords(self):
        return self.top_keywords.query(10)

    def get_top_ips(self):
        return self.top_ips.query(10)

    def get_keyword(self, keyword):
        return self.keywords.query(keyword)
